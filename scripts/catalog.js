import { apiGet } from "./api.js";
import { renderProductCards } from "./product_cards.js";

const container = document.querySelector('#all')
const catalog = container.querySelector('.catalog')
const page_size = 50
let page = 1
const pagination = document.querySelector('.pagination')
const page_next_btn = pagination.querySelector('.pagination_next')
const page_prev_btn = pagination.querySelector('.pagination_prev')
const page_number = pagination.querySelector('span')

setPage(page, page_size, page_number)

page_next_btn.addEventListener('click', () => {
    page += 1
    setPage(page, page_size, page_number)
})

page_prev_btn.addEventListener('click', () => {
    if (page > 1){
        page -= 1
        setPage(page, page_size, page_number)
    }
})


async function getProducts(offset, limit){
    let product_ids = await apiGet('get_ids', {offset: offset, limit: limit})
    let products = await apiGet('get_items', {ids: product_ids})

    let unic_products = products.filter((obj, idx, products) => 
        idx === products.findIndex((t) => t.id === obj.id));
    return unic_products
}

async function setPage(page, page_size, page_number){
    let offset = (page - 1) * page_size
    page_number.innerText = `Страница ${page}`
    catalog.innerHTML = `<div class="d-flex justify-content-center w-100 p-3">
                            <div class="spinner-border text-info" role="status">
                            <span class="visually-hidden">Загрузка...</span>
                            </div>
                        </div>`
    let products = await getProducts(offset, page_size)
    renderProductCards(catalog, products)
}