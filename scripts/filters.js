import { apiGet } from "./api.js";
import { renderProductCards } from "./product_cards.js";

const container = document.querySelector('#filters')
const catalog = container.querySelector('.catalog')

const inputs = container.querySelectorAll('input')
const filter_btn = document.querySelector('.filter')

const filters = {}

inputs.forEach(input => {
    input.addEventListener('input', () => {
        if(input.value){
            filters[input.name] = input.type === 'number'? Number(input.value): input.value
        }
        else{
            delete filters[input.name]
        }
    })

    input.addEventListener('focus', (event) => {
        inputs.forEach(input => {
            if(event.target !== input){
                input.value = ''
                delete filters[input.name]
            }
        })
    })
})

filter_btn.addEventListener('click', async () => {
    if(Object.keys(filters).length){
        catalog.innerHTML = `<div class="d-flex justify-content-center w-100 p-3">
                            <div class="spinner-border text-info" role="status">
                            <span class="visually-hidden">Загрузка...</span>
                            </div>
                        </div>`
        let products = await getProducts(filters)
        renderProductCards(catalog, products)
    }
    else{
        catalog.innerHTML = ''
    }
})

async function getProducts(filters){
    let product_ids = await apiGet('filter', filters)
    let products = await apiGet('get_items', {ids: product_ids})

    let unic_products = products.filter((obj, idx, products) => 
        idx === products.findIndex((t) => t.id === obj.id));
    return unic_products
}