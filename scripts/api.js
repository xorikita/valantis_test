export async function apiGet(action, params){
    let data = {
        action: action,
        params: params,
    }
    
    let date = new Date();
    let timeshtamp = `${date.getUTCFullYear()}${(date.getUTCMonth() + 1).toString().padStart(2, "0")}${date.getUTCDate()}`
    let password = 'Valantis'
    let x_auth = MD5(`${password}_${timeshtamp}`)

    let response
    let result
    let attempt = 0
    let stopped = false

    while(!stopped) {
        response = await fetch('http://api.valantis.store:40000/', {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
                "X-Auth": x_auth,
            },
        })
        result = response.status === 200? await response.json(): console.log(response.status, response.statusText)
        attempt++
        if (result || attempt > 10) stopped = true
    }
    
    return result?.result
}
