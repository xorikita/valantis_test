function renderProductCards(container, data){
    let cards_html = ''
    data.forEach(element => {
        let brand = element.brand? element.brand: '-'
        let price = formatter.format(element.price)
        cards_html += `
            <div class="col-md-4 p-2">
                <div class="card p-0 h-100">
                    <div class="card-header">
                        <small class="text-muted">${element.id}</small>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">${element.product}</h5>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Бренд: ${brand}</li>
                        <li class="list-group-item">Цена: ${price}</li>
                    </ul>
                </div>
            </div>
        `
    });
    container.innerHTML = cards_html
}

const formatter = new Intl.NumberFormat('ru-RU', {
    style: 'currency',
    currency: 'RUB',
  });



export {renderProductCards}