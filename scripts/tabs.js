(function(){
    const containers = document.querySelectorAll('.tabs')

    containers.forEach(container => {
        initTabs(container)
    })

    function initTabs(container){
        const links = container.querySelectorAll('.nav-link')
        const tabs = container.querySelectorAll('.tab')

        links.forEach(link => {
            link.addEventListener('click', () => {
                let current = link.getAttribute('aria')
                tabs.forEach(tab => {
                    if(tab.id === current){
                        tab.classList.remove('d-none')
                    }
                    else{
                        tab.classList.add('d-none')
                    }
                })
                links.forEach(link => {
                    if(link.getAttribute('aria') === current){
                        link.classList.add('active')
                    }
                    else{
                        link.classList.remove('active')
                    }
                })
            })
        })
    }
})()